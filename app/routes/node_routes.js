const parameters = require('../config/parameters');
const Modbus = require('jsmodbus');
const net = require('net');
const axios = require('axios');
const options = {
    'host': parameters.modbusServerIp,
    'port': parameters.modbusClientPort,
}
const mercureServeur = parameters.mercureServeurIp;
const defaultMessage = {
    'success': false,
    'errorMessage': 'Une erreur est survenue'
};
const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJtZXJjdXJlIjp7InN1YnNjcmliZSI6WyIqIl0sInB1Ymxpc2giOlsiKiJdfX0.5hET3gtVVeG84Ne1cQWs-iShwd2Ag8-bU70SeepBLVg';

module.exports = function (app, db) {
    // Configuration de l'entête de la réponse
    app.use(function(req, res, next) {
        res.header("Access-Control-Allow-Origin","*"); 
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
      });
    app.post('/lampe/:id/on', (req, res) => {
        const socket = new net.Socket();
        const client = new Modbus.client.TCP(socket, 1);
        idSubscriber = req.query.subscriberId;      
        idLampe = req.params.id;
        socket.on('connect', function () {
            client.writeSingleCoil(parseInt(idLampe) + parameters.modbusOffset, true)
                .then(function (resp) {
                    const jsonResponse = {
                        "success": true,
                        "friendlyMessage": "Allumage de la lampe n° " + idLampe,
                        "id": idLampe,
                        "offset": parameters.modbusOffset,
                        "idSubscriber":idSubscriber
                    }
                    console.log(jsonResponse.friendlyMessage);                   
                    res.send(jsonResponse) 
                    socket.end()
                }).catch(function () {
                    console.error(arguments);
                    res.send(defaultMessage);
                    socket.end();
                })
        });
        socket.on('error', (error) => {
            handleError(res, error)
        });
        socket.connect(options);

    });

    app.post('/lampe/:id/off', (req, res) => {
        const socket = new net.Socket();
        const client = new Modbus.client.TCP(socket, 1);
        idLampe = parseInt(req.params.id);
        idSubscriber = req.query.subscriberId;      
        socket.on('connect', function () {
            client.writeSingleCoil(parseInt(idLampe) + parameters.modbusOffset, false)
                .then(function (resp) {
                    const jsonResponse = {
                        "success": true,
                        "friendlyMessage": "Extinction de la lampe n° " + idLampe,
                        "id": idLampe,
                        "offset": parameters.modbusOffset
                    }                   
                    console.log(jsonResponse.friendlyMessage);
                    res.send(jsonResponse)     
                    socket.end()                    
                }).catch(function () {
                    console.error(arguments)
                    socket.end();
                })
        });
        socket.on('error', (error) => {
            handleError(res, error)
        });
        socket.connect(options);
    });

    app.get('/lampe/:id', (req, res) => {
        const socket = new net.Socket();
        const client = new Modbus.client.TCP(socket, 1);
        idLampe = parseInt(req.params.id);
        socket.on('connect', function () {
            client.readCoils(parseInt(idLampe) + parameters.modbusOffset, 1)
                .then(function (resp) {
                    const jsonResponse = {
                        "success": true,
                        "friendlyMessage": "Etat de la lampe n° " + idLampe,
                        "id": idLampe,
                        "offset": parameters.modbusOffset,
                        "etat": resp.response._body._coils[0],
                        "response": resp.response,

                    }
                    res.send(jsonResponse);
                    console.log(jsonResponse.friendlyMessage);
                    socket.end()
                }).catch(function () {
                    console.error(arguments)
                    socket.end();
                })
        });
        socket.on('error', (error) => {
            handleError(res, error)
        });
        socket.connect(options);
    });

    app.get('/etat/:id/lampe', (req, res) => {
        const socket = new net.Socket();
        const client = new Modbus.client.TCP(socket, 1);
        idLampe = parseInt(req.params.id);
        socket.on('connect', function () {
            client.readCoils(16 + parseInt(idLampe) + parameters.modbusOffset, 1)
                .then(function (resp) {
                    const jsonResponse = {
                        "success": true,
                        "friendlyMessage": "Retour d'état de la lampe n° " + idLampe,
                        "id": idLampe,
                        "offset": parameters.modbusOffset,
                        "etat": resp.response._body._coils[0],
                        "response": resp.response,

                    }
                    res.send(jsonResponse);
                    console.log(jsonResponse.friendlyMessage);
                    socket.end()
                }).catch(function () {
                console.error(arguments)
                socket.end();
            })
        });
        socket.on('error', (error) => {
            handleError(res, error)
        });
        socket.connect(options);
    });

    app.get('/lampe', (req, res) => {
        const socket = new net.Socket();
        const client = new Modbus.client.TCP(socket, 1);
        idLampe = parseInt(req.params.id);
        socket.on('connect', function () {
            client.readCoils(0, 16)
                .then(function (resp) {
                    const jsonResponse = {
                        "success": true,
                        "friendlyMessage": "Etat des 16 lampes",
                        "id": idLampe,
                        "offset": parameters.modbusOffset,
                        "etat": resp.response._body._coils.reduce((prev, next) => prev + next),
                        "etats": resp.response._body._valuesAsArray,
                        "response": resp.response,

                    }
                    res.send({"etat":  resp.response._body._valuesAsArray});
                    console.log(jsonResponse.friendlyMessage);
                    socket.end()
                }).catch(function () {
                    console.error(arguments)
                    socket.end();
                })
        });
        socket.on('error', (error) => {
            handleError(res, error)
        });
        socket.connect(options);
    });

    app.get('/etat/lampe', (req, res) => {
        const socket = new net.Socket();
        const client = new Modbus.client.TCP(socket, 1);
        idLampe = parseInt(req.params.id);
        socket.on('connect', function () {
            client.readCoils(16, 16)
                .then(function (resp) {
                    const jsonResponse = {
                        "success": true,
                        "friendlyMessage": "Retours d'état des 16 lampes",
                        "id": idLampe,
                        "offset": parameters.modbusOffset,
                        "etat": resp.response._body._coils.reduce((prev, next) => prev + next),
                        "etats": resp.response._body._valuesAsArray,
                        "response": resp.response,

                    }
                    res.send({"etat":  resp.response._body._valuesAsArray});
                    console.log(jsonResponse.friendlyMessage);
                    socket.end()
                }).catch(function () {
                console.error(arguments)
                socket.end();
            })
        });
        socket.on('error', (error) => {
            handleError(res, error)
        });
        socket.connect(options);
    });


    function handleError(res, error) {
        console.error(error);
        defaultMessage['errorMessage'] = error;
        res.send(defaultMessage);
    }
}