'use strict';
const parameters = require('./app/config/parameters.js');
const express = require ('express');
const bodyParser = require('body-parser');
const app = express();
const port = parameters.apiPort;

require('./app/routes')(app,{});
if(parameters.flagUseModbusServer){
    // Démarre un server modbus en local
    require('./app/util/modbusServer')();
}

app.use(bodyParser.urlencoded({extended:true}));

app.listen(port, ()=>{        
    console.log("We are live on "+ port);}
);